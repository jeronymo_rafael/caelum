/* Função que obtem o valor informado pelo usuário através do prompt() 
   e o converte para um float independentemente se ele usou ponto ou vírgula para as casas decimais */
function pegaNumero(mensagem) {
    return parseFloat(prompt(mensagem).replace(",", "."));
}